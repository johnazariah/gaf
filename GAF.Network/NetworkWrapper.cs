﻿/*
	Genetic Algorithm Framework for .Net
	Copyright (C) 2016  John Newcombe

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

		You should have received a copy of the GNU Lesser General Public License
		along with this program.  If not, see <http://www.gnu.org/licenses/>.

	http://johnnewcombe.net
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;


namespace GAF.Network
{

	/// <summary>
	/// This class is a wrapper for the GAF.GeneticAlgorithm class and provides networking funtionality.
	/// </summary>
    public class NetworkWrapper : IDisposable
	{
		private const string ServiceName = "gaf-evaluation-server";
		private string _fitnessAssemblyName;
		private EvaluationClient _evaluationClient;
		private IServiceDiscovery _serviceDiscoveryClient;

        /// <summary>
        /// Delegate definition for the EvaluationComplete event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void EvaluationCompleteHandler(object sender, EvaluationEventArgs e);

        /// <summary>
        /// Event definition for the EvaluationComplete event handler.
        /// </summary>
        public event EvaluationCompleteHandler OnEvaluationComplete;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:GAF.Network.NetworkWrapper"/> class.
		/// </summary>
		/// <param name="geneticAlgorithm">Genetic algorithm.</param>
		/// <param name="serviceDiscoveryClient">Service discovery client.</param>
		/// <param name="fitnessAssemblyName">Fitness assembly name.</param>
		/// <remarks>
		/// Re-initialising the server will, if the fitness function is not defined by the server, 
		/// re-transmit the the fitness function to the server.
		/// </remarks>
        public NetworkWrapper(GAF.GeneticAlgorithm geneticAlgorithm, IServiceDiscovery serviceDiscoveryClient, string fitnessAssemblyName)
            :this(geneticAlgorithm, serviceDiscoveryClient, fitnessAssemblyName, serviceDiscoveryClient.GetActiveServices(ServiceName).Count)
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="T:GAF.Network.NetworkWrapper"/> class.
        /// </summary>
        /// <param name="geneticAlgorithm">Genetic algorithm.</param>
        /// <param name="serviceDiscoveryClient">Service discovery client.</param>
        /// <param name="fitnessAssemblyName">Fitness assembly name.</param>
        /// <param name="concurrentRequests">Concurrent requests.</param>
        public NetworkWrapper (GAF.GeneticAlgorithm geneticAlgorithm, IServiceDiscovery serviceDiscoveryClient, string fitnessAssemblyName, int concurrentRequests)
            :this(geneticAlgorithm,serviceDiscoveryClient.GetActiveServices(ServiceName), fitnessAssemblyName, concurrentRequests)
        {
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="T:GAF.Network.NetworkWrapper"/> class.
        /// </summary>
        /// <param name="geneticAlgorithm">Genetic algorithm.</param>
        /// <param name="endpoints">Endpoints.</param>
        /// <param name="fitnessAssemblyName">Fitness assembly name.</param>
        /// <param name="concurrentRequests">Concurrent requests.</param>
        public NetworkWrapper(GAF.GeneticAlgorithm geneticAlgorithm, List<IPEndPoint> endpoints, string fitnessAssemblyName, int concurrentRequests)
        { 
            if (geneticAlgorithm == null)
                throw new ArgumentNullException(nameof(geneticAlgorithm));

            if (geneticAlgorithm.Population == null)
                throw new NullReferenceException("The specified GeneticAlgorithm.Population object is null.");

            if (geneticAlgorithm.Population.Solutions == null || geneticAlgorithm.Population.Solutions.Count == 0)
                throw new NullReferenceException("The specified GeneticAlgorithm.PopulationSolutions object is null or empty.");

            if (endpoints == null || endpoints.Count() == 0)
                throw new NullReferenceException("The endpoints have not been specified.");

            if (string.IsNullOrEmpty(fitnessAssemblyName))
                throw new NullReferenceException("The specified fitness assembly name is null or empty");

            this.EndPoints = endpoints;

            _fitnessAssemblyName = fitnessAssemblyName;

            //store the referenc to the GA and hook up to the evaluation begin class
            this.GeneticAlgorithm = geneticAlgorithm;
            this.GeneticAlgorithm.Population.OnEvaluationBegin += PopulationOnEvaluationBegin;

            LogEndpoints(EndPoints);

            _evaluationClient = new EvaluationClient(this.EndPoints, _fitnessAssemblyName, concurrentRequests);
            _evaluationClient.OnEvaluationComplete += EvaluationClientOnEvaluationComplete;
        }

		private void PopulationOnEvaluationBegin (object sender, GAF.EvaluationEventArgs args)
		{
			//this event is called each time a population is evaluated therefore
			// this will be called after each operator has been invoked
			try {

				var stopwatch = new Stopwatch ();
				stopwatch.Start ();

				if (args.SolutionsToEvaluate.Count > 0) {

					var evaluations = _evaluationClient.Evaluate (args.SolutionsToEvaluate).Result;

					if (evaluations > 0) {
						args.Evaluations = evaluations;
					} else {
						throw new ApplicationException ("No evaluations undertaken, check that a server exists.");
					}

					stopwatch.Stop ();
					Log.Debug (string.Format ("Evaluation time = {0} ms.", stopwatch.ElapsedMilliseconds));
				}
			} catch (Exception ex) {

				while (ex.InnerException != null) {
					ex = ex.InnerException;
				}

                Log.Error("[GAF.Network.NetworkWrapper.OnEvaluationBegin]", ex);

			} finally {
				//prevent the normal evaluation process from taking place
				args.Cancel = true;
			}
		}

        private void EvaluationClientOnEvaluationComplete(object sender, Network.EvaluationEventArgs e)
        {
            if (OnEvaluationComplete != null)
            {
                Log.Debug(string.Format("Evaluted by server {0}:{1}", e.IPAddress, e.Port));
                this.OnEvaluationComplete(this, e);
            }
        }

		private void LogEndpoints (List<IPEndPoint> endpoints)
		{
			foreach (var endpoint in endpoints) {
                if (endpoint != null)
                {
                    Log.Info(string.Format("  Discovered Endpoint: {0}:{1}", endpoint.Address, endpoint.Port));
                } else {
                    Log.Info("  Endpoint is null");
                }
            }
		}

		/// <summary>
		/// Gets a reference to the 'wrapped' GeneticAlgorithm object.
		/// </summary>
		/// <value>The genetic algorithm.</value>
		public GAF.GeneticAlgorithm GeneticAlgorithm { private set; get; }

		/// <summary>
		/// Gets the end points as retrieved via service discovery.
		/// </summary>
		/// <value>The end points.</value>
		public List<IPEndPoint> EndPoints { private set; get; }

		/// <summary>
		/// Gets the consul node end point. Use this if the local Consul Node is listening
		/// on a different IP/Port to the hosts IP and port 8500.
		/// </summary>
		/// <value>The consul node end point.</value>
		public IPEndPoint ConsulNodeEndPoint { private set; get; }

		/// <summary>
		/// Creates an endpoint from the IP address and Port number as a colon delimetered string.
		/// Parameter must be in the format IPAddress:PortNumber e.g. 192.168.1.64:11000.
		/// </summary>
		/// <returns>The endpoint.</returns>
		/// <param name="endpointAddress">Address with port.</param>
		public static IPEndPoint CreateEndpoint (string endpointAddress)
		{
			IPEndPoint ipEndPoint = null;

			if (endpointAddress.Contains (":")) {

				var epSegments = endpointAddress.Split (":".ToCharArray ());

				IPAddress addr = null;
				int port = 0;

				if (IPAddress.TryParse (epSegments [0], out addr) &&
					int.TryParse (epSegments [1], out port)) {

					ipEndPoint = new IPEndPoint (addr, port);

				}
			}

			return ipEndPoint;
		}

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    _evaluationClient.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~NetworkWrapper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}

