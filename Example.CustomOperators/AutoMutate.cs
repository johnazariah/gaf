﻿using System;
using System.Linq;
using GAF;
using GAF.Operators;

namespace CustomOperators
{
	public class AutoMutate : MutateBase, IGeneticOperator
	{
		private AutoMutateFactor _autoMutationFactorS;
		private readonly object _syncLock = new object ();

		public AutoMutate (double mutationProbability)
			: base (mutationProbability)
		{
		}

		protected override void Mutate (Chromosome child)
		{
			//store the defined mutation probability
			var tempProbability = MutationProbability;

			//adjust and scale for AutoMutate Factor based on the value of the last gene
			var nonPhenotypeGene = child.Genes.Last ();

			if (nonPhenotypeGene.BinaryValue == 1) {
				MutationProbability = MutationProbability * (int)AutoMutationFactor;
			}

			base.Mutate (child);

			//restore the original probability
			MutationProbability = tempProbability;

		}

		protected override void MutateGene (Gene gene)
		{
			// This example mutates Binary, Real and Integer types and raises 
			// an exception if the Gene is any other type.

			if (gene.GeneType == GeneType.Binary) {
				gene.ObjectValue = !(bool)gene.ObjectValue;
			} else {
				throw new OperatorException ("Genes with this GeneType cannot be mutated by this operator.");
			}
		}

		public AutoMutateFactor AutoMutationFactor {
			get {
				lock (_syncLock) {
					return _autoMutationFactorS;
				}
			}
			set {
				lock (_syncLock) {
					_autoMutationFactorS = value;
				}
			}
		}
	}
}

