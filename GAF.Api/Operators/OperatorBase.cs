﻿using System;
using System.ComponentModel;

namespace GAF.Api.Operators
{
	public class OperatorBase : GafApiBase, IOperator
	{
		protected readonly GAF.IGeneticOperator _operator;
		private bool _enabled;
		private bool _requiresEvaluatedPopulation;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:GAF.Api.Operators.OperatorBase"/> class.
		/// </summary>
		/// <param name="geneticOperator">Genetic operator.</param>
		public OperatorBase(IGeneticOperator geneticOperator)
		{
			if (geneticOperator == null) {
				throw new NullReferenceException ("The IGeneticOperator object is null;");
			}
			_operator = geneticOperator;
			this.Enabled = _operator.Enabled;
			this.RequiresEvaluatedPopulation = _operator.RequiresEvaluatedPopulation;
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:GAF.Api.Operators.OperatorBase"/> is enabled.
		/// </summary>
		/// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
		public bool Enabled {
			set {
				if (UpdateField (ref _enabled, value, "Enabled")) {
					_operator.Enabled = value;
				}
			}
			get {
				return _operator.Enabled;

			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:GAF.Api.Operators.OperatorBase"/> requires an evaluated population.
		/// </summary>
		/// <value><c>true</c> if requires evaluated population; otherwise, <c>false</c>.</value>
		public bool RequiresEvaluatedPopulation {
			set {
				if (UpdateField (ref _requiresEvaluatedPopulation, value, "RequiresEvaluatedPopulation")) {
					_operator.RequiresEvaluatedPopulation = value;
				}
			}
			get {
				return _operator.RequiresEvaluatedPopulation;

			}
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>The description.</value>
		public virtual string Description {
			get {
				throw new NotImplementedException ();
			}
			protected set {
				throw new NotImplementedException ();
			}
		}
	}
}

